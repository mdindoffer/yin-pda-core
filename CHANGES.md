#Changelog
##1.1.2
* Fix very rare arithmetic underflow errors
* Enable JDK 7 and 8 builds
##1.1.1
* Add missing hashcode/equals to F0Candidate
##1.1.0
* Return the confidence value (d'(T)) of the final estimate 
##1.0.1
* Weaken type constraints in Yin
* Add missing toString methods
* Correct builds for JDK7 targets on newer JDKs
##1.0.0
Initial release