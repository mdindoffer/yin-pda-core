package eu.dindoffer.yin.pda.core.api;

/**
 *
 * @author Martin Dindoffer contact@dindoffer.eu
 */
public interface YinAlgorithm {

    /**
     * Calculates the length of F0 period on a window starting from given offset of the buffer.
     * If the integration window overlaps the buffer end, it is aligned to fit.
     *
     * @param offset offset of a window from the start of the audio buffer (in samples)
     * @return f0 estimation candidate containing the length of the F0 period in samples
     */
    F0Candidate calculatePitch(int offset);

}
