package eu.dindoffer.yin.pda.core.api;

/**
 *
 * @author Martin Dindoffer
 */
public final class YinUtils {

    private YinUtils() {
    }

    /**
     * Calculates minimal size of integration window for given tauMax.
     * <p>
     * The minimal size is basically 2.5 * tauMax. It is required for the Yin algorithm to search
     * for the maximum period in an audio buffer.
     * It, however, doesn't include the amount of samples before the estimation point
     * used in local neighborhood search, which are defined as tauMax / 2.
     *
     * @param tauMax maximum period to look for
     * @return minimal size of YIN integration window (in samples)
     */
    public static int calculateMinimalIntegrationWindowSize(int tauMax) {
        if (isEven(tauMax)) {
            return (int) (2.5 * tauMax) + 1;
        } else {
            return (int) Math.ceil(2.5 * tauMax);
        }
    }

    private static boolean isEven(int number) {
        return (number % 2) == 0;
    }
}
