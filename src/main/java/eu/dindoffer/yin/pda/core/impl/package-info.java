/**
 * Core implementation of the YIN algorithm. Can be used directly or subclassed by consumers for specialized usage.
 */
package eu.dindoffer.yin.pda.core.impl;
