package eu.dindoffer.yin.pda.core.api;

/**
 * Parameters of the YIN estimation algorithm
 *
 * @author Martin Dindoffer
 */
public final class YinSettings {

    private final double threshold;
    private final int taumax;//maximum period length, 2*taumax == window size
    private final double deviation;

    private YinSettings(double threshold, int taumax, double deviation) {
        this.threshold = threshold;
        this.taumax = taumax;
        this.deviation = deviation;
    }

    /**
     * @return threshold value for candidate selection
     */
    public double getThreshold() {
        return threshold;
    }

    /**
     * @return largest period to look for (in samples)
     */
    public int getTaumax() {
        return taumax;
    }

    /**
     * @return allowed deviation of final estimate from the initial one (iow coefficient of the initial estimate)
     */
    public double getDeviation() {
        return deviation;
    }

    @Override
    public String toString() {
        return "YinSettings{" +
                "threshold=" + threshold +
                ", taumax=" + taumax +
                ", deviation=" + deviation +
                '}';
    }

    public static class Builder {

        private double threshold = 0.1;
        private int taumax = 1696; //cca 26 Hz
        private double deviation = 0.2;

        /**
         * @param threshold threshold value for candidate selection, defaults to 0.1
         * @return builder
         */
        public Builder setThreshold(double threshold) {
            this.threshold = threshold;
            return this;
        }

        /**
         * @param taumax largest period to look for, defaults to 1696 ~cca 26Hz
         * @return builder
         */
        public Builder setTaumax(int taumax) {
            this.taumax = taumax;
            return this;
        }

        /**
         * @param deviation allowed deviation of final estimate from the initial one
         *                  (iow coefficient of the initial estimate), defaults to 0.2
         * @return builder
         */
        public Builder setDeviation(double deviation) {
            this.deviation = deviation;
            return this;
        }

        public YinSettings build() {
            return new YinSettings(threshold, taumax, deviation);
        }

    }
}
