package eu.dindoffer.yin.pda.core.api;

/**
 * Period estimate candidate.
 */
public class F0Candidate {

    private final double tau;//Period length
    private final double value;//Normalized function value

    /**
     * Create a new candidate with given value of d'(T).
     *
     * @param tau   period of the candidate in samples
     * @param value normalized value of d'(T) (confidence)
     */
    public F0Candidate(double tau, double value) {
        this.tau = tau;
        this.value = value;
    }

    /**
     * Get F0 period in samples.
     *
     * @return F0 period in samples
     */
    public double getTau() {
        return tau;
    }

    /**
     * Get the normalized value of d'(T).
     *
     * @return normalized value of d'(T) (i.e. the confidence)
     */
    public double getValue() {
        return value;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (!(obj instanceof F0Candidate)) return false;
        F0Candidate that = (F0Candidate) obj;
        if (Double.compare(that.tau, tau) != 0) return false;
        return Double.compare(that.value, value) == 0;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        temp = Double.doubleToLongBits(tau);
        result = (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(value);
        result = (31 * result) + (int) (temp ^ (temp >>> 32));
        return result;
    }

    @Override
    public String toString() {
        return "F0Candidate{" +
                "tau=" + tau +
                ", value=" + value +
                '}';
    }

}
