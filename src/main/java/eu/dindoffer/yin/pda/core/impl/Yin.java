package eu.dindoffer.yin.pda.core.impl;

import eu.dindoffer.yin.pda.core.api.F0Candidate;
import eu.dindoffer.yin.pda.core.api.YinAlgorithm;
import eu.dindoffer.yin.pda.core.api.YinSettings;

import java.util.Collection;
import java.util.Deque;
import java.util.LinkedList;

/**
 * YIN pitch detection algorithm
 *
 * @author Martin Dindoffer
 */
public class Yin implements YinAlgorithm {

    private final short[] buffer;
    private final double threshold;
    private final int taumax;//maximum period length, 2*taumax == window size
    private final double deviation;

    /**
     * Creates a new algorithm instance with given settings.
     *
     * @param buffer   decoded audio samples
     * @param settings parameters of the YIN algorithm
     */
    public Yin(short[] buffer, YinSettings settings) {
        this(buffer, settings.getThreshold(), settings.getTaumax(), settings.getDeviation());
    }

    /**
     * Creates a new algorithm instance with given parameters.
     *
     * @param audioBuffer decoded audio samples
     * @param threshold   threshold value for candidate selection
     * @param tauMax      largest period to look for
     * @param deviation   allowed deviation of final estimate from the initial one
     *                    (iow coefficient of the initial estimate)
     */
    public Yin(short[] audioBuffer, double threshold, int tauMax, double deviation) {
        if ((tauMax == 0) || (deviation < 0)) {
            throw new IllegalArgumentException("Illegal values of taumax or deviation provided.");
        }
        this.buffer = audioBuffer;
        this.taumax = tauMax;
        this.threshold = threshold;
        this.deviation = deviation;
    }

    /**
     * Calculates the length of F0 period on a window starting from given offset of the buffer.
     * If the integration window overlaps the buffer end, it is aligned to fit.
     *
     * @param offset offset of a window from the start of the audio buffer (in samples)
     * @return f0 estimation candidate containing the length of the F0 period in samples,
     *         or a candidate with period and value of zero, if no pitch is found
     */
    @Override
    public F0Candidate calculatePitch(int offset) {
        boolean leftmostWindowCalculation;
        if (offset == 0) {//we won't be calculating the F0 shifted to the left, only to the right
            leftmostWindowCalculation = true;
        } else {
            int temp = (int) Math.ceil(buffer.length - 2.5 * taumax);
            if (offset >= temp) {//if we are near the end of the buffer, align it
                offset = temp - 1;
            }
            leftmostWindowCalculation = false;
        }

        //firstly let's find the initial estimate
        long[] initDif = initialDifferenceFunc(offset);
        double[] initNorm = normalized(initDif, taumax);
        Deque<F0Candidate> initialCandidates = interpolate(initDif, initNorm, 1);
        F0Candidate initialEstimate = initialAbsThreshold(initialCandidates);
        if (initialEstimate == null) {
            return new F0Candidate(0, 0);
        }

        //now look at the neighbours
        int lowerBound = (int) (initialEstimate.getTau() * (1 - deviation));
        int upperBound = (int) (initialEstimate.getTau() * (1 + deviation));
        if (upperBound > taumax) {
            upperBound = taumax;
        }
        if (lowerBound < 1) {
            lowerBound = 1;
        }
        if (upperBound == lowerBound) {
            return initialEstimate;
        }

        long[][] surroundingDiffs = differenceFunctions(offset, leftmostWindowCalculation, upperBound);
        double[][] surroundingNorms = new double[surroundingDiffs.length][];
        for (int i = 0; i < surroundingNorms.length; i++) {
            surroundingNorms[i] = normalized(surroundingDiffs[i], upperBound);
        }
        Collection<LinkedList<F0Candidate>> wholeList = new LinkedList<>();
        for (int t = 0; t < surroundingDiffs.length; t++) {
            LinkedList<F0Candidate> partialList = interpolate(surroundingDiffs[t], surroundingNorms[t], lowerBound);
            if (!partialList.isEmpty()) {
                wholeList.add(partialList);
            }
        }
        return absoluteThreshold(wholeList);
    }

    /**
     * Calculates the difference function in full tauMax range on given offset.
     *
     * @param offset window offset from the buffer beginning
     * @return difference function values on given offset
     */
    protected long[] initialDifferenceFunc(int offset) {
        long[] initDif = new long[taumax + 1];// taumax is actually the largest period, so +1 for tau=0
        //calculation of the first one must be done in its entirety
        for (int tau = 1; tau <= taumax; tau++) {//for tau=0 the difference function always equals to 0
            long sum = 0L;
            for (int j = 0; j <= taumax; j++) {
                long temp = buffer[offset + j] - (long) buffer[offset + j + tau];//cast to long before the operation to prevent underflow
                temp *= temp;
                sum += temp;
            }
            initDif[tau] = sum;
        }
        return initDif;
    }

    /**
     * Normalizes a given difference function with cumulative mean.
     *
     * @param difference difference function supplied to normalize
     * @param upperBound maximum period to look for
     * @return normalized function
     */
    protected double[] normalized(long[] difference, int upperBound) {
        double[] norm = new double[upperBound + 1];
        norm[0] = 1;

        double[] sum = new double[upperBound + 1];
        sum[0] = difference[0];
        for (int i = 1; i <= upperBound; i++) {
            sum[i] = sum[i - 1] + difference[i];
        }
        for (int tau = 1; tau <= upperBound; tau++) {
            // multiplication is x-times faster than division, so let's move tau to the numerator
            norm[tau] = (difference[tau] * tau) / sum[tau];
        }

        return norm;
    }

    /**
     * Finds all the local minima of the normalized function and picks the cadidates
     * for final period via parabolic interpolation.
     * If a candidate is found, which falls below the threshold, the search is
     * stopped prematurely speeding the process up.
     *
     * @param differenceFunc difference function, from which are interpolated abscissas of the candidates
     * @param normalizedFunc normalized function, from which are interpolated ordinates of the candidates
     * @param lowerBound     minimal period to look for
     * @return list of all the candidates for final period
     */
    protected LinkedList<F0Candidate> interpolate(long[] differenceFunc, double[] normalizedFunc, int lowerBound) {
        LinkedList<F0Candidate> list = new LinkedList<>();
        double previous = normalizedFunc[lowerBound - 1];
        boolean descending = false;
        for (int i = lowerBound; i < normalizedFunc.length - 1; i++) {
            if (previous > normalizedFunc[i]) {
                descending = true;
            }
            if (descending) {
                if (normalizedFunc[i] < normalizedFunc[i + 1]) {
                    // local minimum must be at the same place even on the difference function,
                    // otherwise it is possible to try and interpolate e.g. ascending points,
                    // which leads to incorrect values
                    if ((differenceFunc[i - 1] >= differenceFunc[i]) && (differenceFunc[i] <= differenceFunc[i + 1])) {
                        double y = interpolateNorm(normalizedFunc[i - 1], normalizedFunc[i], normalizedFunc[i + 1]);
                        double x = interpolateDif(differenceFunc[i - 1], differenceFunc[i], differenceFunc[i + 1], i - 1);
                        F0Candidate temp = new F0Candidate(x, y);
                        list.add(temp);
                        descending = false;
                        if (y < threshold) {
                            break;
                        }
                    }
                }
            }
            previous = normalizedFunc[i];
        }
        return list;
    }

    /**
     * Calculates the ordinate of the parabola vertex created by interpolating given ordinates of neighboring points.
     *
     * @param y1 ordinate of the first (left) point
     * @param y2 ordinate of the second (middle) point
     * @param y3 ordinate of the third (right) point
     * @return ordinate of the parabola vertex
     */
    protected double interpolateNorm(double y1, double y2, double y3) {
        double b = ((y3 - (4 * y2)) + (3 * y1)) / -2.0;
        double a = y2 - b - y1;
        double vertexX = -b / (2 * a);
        return (a * vertexX * vertexX) + (b * vertexX) + y1;
    }

    /**
     * Calculates the abscissa of the parabola vertex created by interpolating given ordinates of neighboring points
     *
     * @param y1 ordinate of the first (left) point
     * @param y2 ordinate of the second (middle) point
     * @param y3 ordinate of the third (right) point
     * @param x1 abscissa of the first (left) point
     * @return abscissa of the parabola vertex
     */
    protected double interpolateDif(long y1, long y2, long y3, int x1) {
        double b = ((y3 - (4 * y2)) + (3 * y1)) / -2.0;
        double a = y2 - b - y1;
        double xVertex = -b / (2 * a);
        return xVertex + x1;
    }

    /**
     * Picks a final period candidate from the list of candidates based on
     * threshold value and normalized function value.
     *
     * @param candidates candidate list
     * @return final candidate, null if the list is empty
     */
    protected F0Candidate initialAbsThreshold(Deque<F0Candidate> candidates) {
        if (candidates.isEmpty()) {
            return null;
        }
        F0Candidate minimum = candidates.peekFirst();
        for (F0Candidate candidate : candidates) {
            if (candidate.getValue() < threshold) {
                return candidate;
            }
            if (candidate.getValue() < minimum.getValue()) {
                minimum = candidate;
            }
        }
        return minimum;
    }

    /**
     * Calculates the difference functions at the given offset and it's neighborhood +-Taumax/2.
     * To speed up the calculation a recursive approach is used,
     * where there's an old value subtracted and a new added in each step.
     *
     * @param offset                    offset of the window from the beginning of the buffer
     * @param leftmostWindowCalculation when true, won't calculate difference functions
     *                                  for integration windows before (to the left of) the offset
     * @param upperBound                maximal period to look for
     * @return difference functions at the given offset and it's neighborhood +-Taumax/2
     */
    protected long[][] differenceFunctions(int offset, boolean leftmostWindowCalculation, int upperBound) {
        int leftOffset;
        int numOfFunctions;
        if (!leftmostWindowCalculation) {
            leftOffset = offset - (taumax / 2);
            if ((taumax % 2) > 0) {
                numOfFunctions = taumax;
            } else {
                numOfFunctions = taumax + 1;
            }
        } else {
            leftOffset = offset;
            numOfFunctions = 1 + (taumax / 2);
        }
        // upperBound is actually the largest period, so +1 for tau==0
        long[][] dif = new long[numOfFunctions][upperBound + 1];

        //calculation of the first (leftmost) function must be done in its entirety
        //we'll store the equation values for each tau (period candidate) of the null shift (j)
        long[] firstValues = new long[upperBound];// warning, array index 0 is for tau==1, microoptimisation ftw
        for (int tau = 1; tau <= upperBound; tau++) {//for tau=0 the difference function always equals to 0
            long sum = 0L;
            boolean first = true;
            for (int j = 0; j <= upperBound; j++) {
                long temp = buffer[leftOffset + j] - (long) buffer[leftOffset + j + tau];//cast to long before the operation to prevent underflow
                temp *= temp;
                if (first) {
                    firstValues[tau - 1] = temp;
                    first = false;
                }
                sum += temp;
            }
            dif[0][tau] = sum;
        }

        for (int funcNr = 1; funcNr < numOfFunctions; funcNr++) {
            int t = leftOffset + funcNr;//current offset (start time of a function)
            int tPlusTaumax = t + upperBound; // this should spare some additions
            for (int tau = 1; tau <= upperBound; tau++) {
                long temp = buffer[tPlusTaumax] - (long) buffer[tPlusTaumax + tau];//cast to long before the operation to prevent underflow
                long newTermToAdd = temp * temp;
                dif[funcNr][tau] = dif[funcNr - 1][tau] - firstValues[tau - 1] + newTermToAdd;
                temp = buffer[t] - (long) buffer[t + tau];//cast to long before the operation to prevent underflow
                firstValues[tau - 1] = temp * temp;
            }
        }
        return dif;
    }

    /**
     * Picks a final period candidate from the list of candidates based on
     * threshold value and normalized function value.
     *
     * @param candidates candidate list in their respective times
     * @return length of the final period, 0 if no period was found
     */
    protected F0Candidate absoluteThreshold(Collection<LinkedList<F0Candidate>> candidates) {
        if (candidates.isEmpty()) {
            return new F0Candidate(0, 0);
        }
        Deque<F0Candidate> finalists = new LinkedList<>();
        for (Deque<F0Candidate> list : candidates) {
            finalists.add(initialAbsThreshold(list));
        }
        F0Candidate min = finalists.peekFirst();
        for (F0Candidate finalist : finalists) {
            if (finalist.getValue() < min.getValue()) {
                min = finalist;
            }
        }
        return min;
    }

    @Override
    public String toString() {
        return "Yin{" +
                "threshold=" + threshold +
                ", taumax=" + taumax +
                ", deviation=" + deviation +
                '}';
    }
}
