package eu.dindoffer.yin.pda.core;

/**
 * A simple sine wave generator.
 */
public class SineWaveGenerator {

    private static final double TWO_PI_DGS = 360.0;

    /**
     * Generate a 16-bit sampled (co)sine wave.
     *
     * @param signalLength    length of the generated signal, in samples
     * @param periodInSamples length of the sine period, in samples
     * @param startPhaseAngle phase angle of the sine function at the beginning. 0 = sine, 90 = cosine
     * @param amplitude       maximum amplitude of the wave
     * @return an array of sampled sine wave
     */
    public static short[] generateSineWave(int signalLength, int periodInSamples, double startPhaseAngle, short amplitude) {
        short[] output = new short[signalLength];
        double waveSamplingStepDegrees = TWO_PI_DGS / periodInSamples;
        for (int i = 0; i < signalLength; i++) {
            double value = StrictMath.sin(StrictMath.toRadians((waveSamplingStepDegrees * i) + startPhaseAngle));
            output[i] = (short) (value * amplitude);
        }
        return output;
    }
}
