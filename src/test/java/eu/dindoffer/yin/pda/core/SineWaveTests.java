package eu.dindoffer.yin.pda.core;

import eu.dindoffer.yin.pda.core.api.F0Candidate;
import eu.dindoffer.yin.pda.core.api.YinSettings;
import eu.dindoffer.yin.pda.core.api.YinUtils;
import eu.dindoffer.yin.pda.core.impl.Yin;
import org.junit.Assert;
import org.junit.Test;

/**
 * A couple of IT tests for testing the YIN implementation.
 * Due to the heuristic and imperfect nature of the algorithm, only approximately correct results are enforced.
 */
public class SineWaveTests {

    private static final int PIANO_TAU_MAX = 1696; //Circa 26Hz for 44.1khz
    private static final double POINT_PERCENT_COEF = 0.1;

    @Test
    public void testSimpleSine() {
        int functionLength = YinUtils.calculateMinimalIntegrationWindowSize(PIANO_TAU_MAX);
        int functionPeriod = 100;
        short[] wave = SineWaveGenerator.generateSineWave(functionLength, functionPeriod, 0, Short.MAX_VALUE);
        YinSettings yinSettings = new YinSettings.Builder().build();
        Yin yin = new Yin(wave, yinSettings);
        F0Candidate f0Candidate = yin.calculatePitch(0);
        Assert.assertTrue("Estimated period differs from the specified one by more than allowed threshold",
                (f0Candidate.getTau() - functionPeriod) < (functionPeriod * POINT_PERCENT_COEF));
    }

}