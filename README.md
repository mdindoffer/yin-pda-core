# YIN Pitch Detection Algorithm Core #

Implements standard version of the YIN pitch detection algorithm as specified in the famous paper by De Cheveigné and
Kawahara in 2002 - [http://audition.ens.fr/adc/pdf/2002_JASA_YIN.pdf](http://audition.ens.fr/adc/pdf/2002_JASA_YIN.pdf)

This is most probably the only full and correct implementation of the algorithm out there.

For an easy to use YIN library go to [yin-pda-lib](https://bitbucket.org/mdindoffer/yin-pda-lib).

#Implementation Details

##Tunable parameters

* Threshold
* TauMax
* Neighbourhood deviation coeficient

##Integration Window

The algorithm implements the time-neighbourhood search as specified in the step 6 of the YIN whitepaper.
The *\[t - tauMax/2\]* part of the search can be ommitted if not enough data are present,
but the other part (+) of the search interval is mandatory. Thus the provided audio must be at least 2.5 times longer
than the TauMax (largest period).
For a full range search, an audio of at least 3 times the length of tauMax is required.

##Known Limitations

* Accepts only 16b audio

#Usage Example

```java
...
short[] audio = getAudio(); //obtain audio samples from somewhere
YinSettings settings = new YinSettings.Builder()
                    .setDeviation(0.3)
                    .setTaumax(1700)
                    .setThreshold(0.2)
                    .build();
Yin yin = new Yin(audio, settings);
F0Candidate fundamental = yin.calculatePitch(1800); //calculate F0 period length at sample n. 1800
System.out.println("F0 period length (samples): " + fundamental.getTau());
System.out.println("F0 estimate confidence score: " + fundamental.getValue());
...
```

#Platform

* Requires at least Java 7
* OSGi compatible
* Android friendly

#Maven
```xml
<dependency>
    <groupId>eu.dindoffer</groupId>
    <artifactId>yin-pda-core</artifactId>
    <version>1.1.2</version>
</dependency>
```

#License
Licensed under the MIT license.

#Author & Contact
I (Martin Dindoffer) created this library in my precious free time just for fun.
If you like it, have a feature request, or just want to buy me a beer, drop me an email at contact_ta_dindoffer.eu